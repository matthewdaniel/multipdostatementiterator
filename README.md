# multiPDOStatementIterator

Itterate over multiple pdo statements.

Sometimes it is terribly complex to JOIN through what you need or you want to avoid unions. This class will grab all records from the first statement until a new value for the key is reached at which point it will switch to the subsequent statements. Once at the end it will increment/decrement the key to the next value and restart the process.

## Example
	$conn = new PDO('mysql:host=localhost;dbname=csproofing.coursestage.local', 'root', 'password');

	$recordset1 = $conn->query("SELECT userid, coursename, attemptnumber FROM course_attempts order by userid DESC");
	$recordset2 = $conn->query("SELECT userid, planname, attemptnumber, plan components FROM learning_plan_attempts order by userid DESC");

	$test = new multiPDOStatementIterator('userid', multiPDOStatementIterator::DESC, $recordset1, $recordset2);

	$userid = false;
	foreach ($test as $record) {
	    if ($userid != $record->userid) {
	    	$userid = $record->userid;
	    	//print header
	    }

	    // print record to report
	}
